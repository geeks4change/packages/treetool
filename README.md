# TreeTool

The ~~*swiss army*~~ **bread and butter knife** for **.json** and **.yml** files.

## Install

```bash
composer require geeks4change/treetool
```

## Examples

```bash
$ # Add a patch entry to composer.json.
$ # --file/-f does in-place editing and is shorthand for --in/-i & --out/-o.
$ bin/tt set -f composer.json extra patches drupal/core "My cool patch" https://drupal.org/files/my-cool.patch

$ # Let's see what this did. Use --in/-i instead of -f to read from file and write to stdout.
$ bin/tt get -i composer.json extra
{
    "patches": {
        "drupal/core": {
            "My cool patch": "https://drupal.org/files/my-cool.patch"
        }
    }
}

$ # Note that 'get' does not have -f/--file in-place edit option to protect you.
$ bin/tt get -f composer.json extra patches drupal/core
  The "-f" option does not exist.  

$ # Of course with a mighty tool you can make mighty mistakes. But you use VCS, don't you?
$ # Or you can preview a change with -i, and only then apply it with -f.
$ # Without the --replace option, 'set' refuses to overwrite arrays with values and values with arrays..
$ bin/tt set -f composer.json extra "This text replaces the complete extra subtree."
 [ERROR] Can not replace array at key extra with value without --replace/-r.                                            

$ # We can later remove the patch.
$ bin/tt del -f composer.json extra patches drupal/core "My coool patch"
$ # When we look what we've got, we see some leftover empty arrays.
$ bin/tt get -i composer.json extra
{
    "patches": {
        "drupal/core": []
    }
}

$ # This need not be. Del command can clean up empty arrays.
$ bin/tt del -f composer.json extra patches drupal/core "My coool patch" --clean
$ bin/tt get -i composer.json extra
null
```

## Edit Json as Yaml

```bash
$ # Yaml is a lot easier to edit:
$ bin/tt edit composer.json
```

## Advanced examples

```bash
$ # Let's see our patch as --out-type=yml
$ bin/tt get -i composer.json extra -u yml
patches:
  drupal/core:
    'My coool patch': 'https://drupal.org/files/my-cool.patch'

$ # I want to extract that patch for my script.
$ bin/tt get -i composer.json extra patches drupal/core "My coool patch"
"https://drupal.org/files/my-cool.patch"

$ # But i want that value without the quotes. And error code 0 for success.
$ bin/tt get -i composer.json extra patches drupal/core "My coool patch" -u plain
https://drupal.org/files/my-cool.patch
$ echo $?
0

$ bin/tt get -i composer.json extra patches drupal/core "No patch here" -u plain
 [ERROR] Result is not a string or number.                                                                              
$ echo $?
5


$ # For an empty set of keys, the provided value will replace the root element.
$ bin/tt set -i composer.json "This string replaces everything" --replace
"This string replaces everything"

$ # You can do pipes. Excessive pipes.
$ echo '' | bin/tt set foo bar baz value | bin/tt set foo bong boom whatnot | bin/tt del foo bar baz  
foo:
  bar: {  }
  bong:
    boom: whatnot

$ # You can leverage the 'type' command to get the type of any data.
$ export data='{"foo": 17, "bar": 42}'; echo $data|bin/tt type
object
$ export data='{"foo": 17, "bar": 42}'; echo $data|bin/tt get foo|bin/tt type
number

$ If you are nerdy enough, you can leverage the 'keys' command to write an XSLT processor in bash.
$ export data='{"foo": 17, "bar": 42}'; echo $data|bin/tt keys|while IFS="\n" read -r k; do export k; v=$(echo "$data" |bin/tt get "$k"); echo "Key $k has value $v"; done
Key foo has value 17
Key bar has value 42

```

## Help
```bash
bin/tt help get
bin/tt help set
bin/tt help del
bin/tt help type
bin/tt help keys
```

## Contributing

If you want to contribute PHPUnit tests, Gitlab CI testing, Phar creation, or a Rust implementation,
go ahead and submit a PR, it will be appreciated.

For everything else, submit an issue so we can see how it fits the bread-and-butter nature of this project.

