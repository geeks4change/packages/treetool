<?php

namespace geeks4change\treetool\Utility;

/**
 * Provides helpers to perform operations on nested data of variable depth.
 * @internal
 */
final class NestedData {

  /**
   * Retrieves a value from a nested array with variable depth.
   *
   * @param object|array|string|int|float|null $array
   *   The array from which to get the value.
   * @param array $parents
   *   An array of parent keys of the value, starting with the outermost key.
   * @param bool $key_exists
   *   (optional) If given, an already defined variable that is altered by
   *   reference.
   *
   * @return object|array|string|int|float|null
   *   The requested nested value. Possibly NULL if the value is NULL or not all
   *   nested parent keys exist. $key_exists is altered by reference and is a
   *   Boolean that indicates whether all nested parent keys exist (TRUE) or not
   *   (FALSE). This allows to distinguish between the two possibilities when
   *   NULL is returned.
   *
   * @see NestedData::setValue()
   * @see NestedData::unsetValue()
   */
  public static function &getValue(&$array, array $parents, &$key_exists = NULL) {
    $ref = &$array;
    foreach ($parents as $parent) {
      if (is_array($ref) && (isset($ref[$parent]) || array_key_exists($parent, $ref))) {
        $ref = &$ref[$parent];
      }
      elseif (is_object($ref) && (isset($ref->$parent) || property_exists($ref, $parent))) {
        $ref = &$ref->$parent;
      }
      else {
        $key_exists = FALSE;
        $null = NULL;
        return $null;
      }
    }
    $key_exists = TRUE;
    return $ref;
  }

  /**
   * Sets a value in a nested array with variable depth.
   *
   * @param object|array|string|int|float|null $array
   *   A reference to the array to modify.
   * @param array $parents
   *   An array of parent keys, starting with the outermost key.
   * @param mixed $value
   *   The value to set.
   * @param bool $replace
   *   (optional) If TRUE, the value is forced into the structure even if it
   *   requires the deletion of an already existing non-array parent value. If
   *   FALSE, PHP throws an error if trying to add into a value that is not an
   *   array. Defaults to FALSE.
   *
   * @see NestedData::unsetValue()
   * @see NestedData::getValue()
   */
  public static function setValue(&$array, array $parents, $value, $replace = FALSE) {
    $ref = &$array;
    foreach ($parents as $parent) {
      // PHP auto-creates container arrays and NULL entries without error if $ref
      // is NULL, but throws an error if $ref is set, but not an array.
      if (isset($ref) && !is_array($ref) && !is_object($ref)) {
        if ($replace) {
          $ref = new \stdClass();
        }
        else {
          throw new \RuntimeException("Can not replace scalar at key $parent with array without --replace/-r.");
        }
      }
      elseif (is_array($ref)) {
        $ref = &$ref[$parent];
      }
      else {
        if (!is_object($ref)) {
          $ref = new \stdClass();
        }
        $ref = &$ref->$parent;
      }
    }
    $ref = $value;
  }

  /**
   * Unsets a value in a nested array with variable depth.
   *
   * @param mixed $array
   *   A reference to the array to modify.
   * @param array $parents
   *   An array of parent keys, starting with the outermost key and including
   *   the key to be unset.
   * @param bool $key_existed
   *   (optional) If given, an already defined variable that is altered by
   *   reference.
   *
   * @see NestedData::setValue()
   * @see NestedData::getValue()
   */
  public static function unsetValue(&$array, array $parents, &$key_existed = NULL) {
    if (!$parents) {
      $array = [];
    }
    $unset_key = array_pop($parents);
    $ref = &self::getValue($array, $parents, $key_existed);
    if ($key_existed && is_array($ref) && (isset($ref[$unset_key]) || array_key_exists($unset_key, $ref))) {
      $key_existed = TRUE;
      unset($ref[$unset_key]);
    }
    if ($key_existed && is_object($ref) && (isset($ref->$unset_key) || property_exists($ref, $unset_key))) {
      $key_existed = TRUE;
      unset($ref->$unset_key);
    }
    else {
      $key_existed = FALSE;
    }
  }

}
