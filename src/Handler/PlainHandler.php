<?php

namespace geeks4change\treetool\Handler;

/**
 * Class StringHandler
 * @internal
 */
final class PlainHandler implements HandlerInterface {

  public function parse(string $raw) {
    return $raw;
  }

  public function dump($data): string {
    $isStringOrNumber = is_string($data) || is_float($data) || is_int($data);
    if (!$isStringOrNumber) {
      throw new \RuntimeException('Result is not a string or number.');
    }
    return $data;
  }

}
