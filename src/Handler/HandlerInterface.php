<?php

namespace geeks4change\treetool\Handler;

/**
 * Interface HandlerInterface
 * @internal
 */
interface HandlerInterface {

  public function parse(string $raw);

  public function dump($data): string;

}
