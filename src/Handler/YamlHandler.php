<?php

namespace geeks4change\treetool\Handler;

use Symfony\Component\Yaml\Yaml as SymfonyYaml;

/**
 * Class YamlHandler
 * @internal
 */
final class YamlHandler implements HandlerInterface {

  public function parse(string $raw) {
    try {
      $data = SymfonyYaml::parse($raw, SymfonyYaml::PARSE_OBJECT_FOR_MAP|SymfonyYaml::PARSE_EXCEPTION_ON_INVALID_TYPE);
    } catch (\Throwable $e) {
      throw new \RuntimeException("Error parsing data: {$e->getMessage()}");
    }
    return $data;
  }

  public function dump($data): string {
    try {
      $data = SymfonyYaml::dump($data, 9999, 2, SymfonyYaml::DUMP_OBJECT_AS_MAP|SymfonyYaml::DUMP_EXCEPTION_ON_INVALID_TYPE);
    } catch (\Throwable $e) {
      throw new \RuntimeException("Error parsing data: {$e->getMessage()}");
    }
    return $data;
  }

}
