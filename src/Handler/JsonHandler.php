<?php

namespace geeks4change\treetool\Handler;

/**
 * Class JsonHandler
 * @internal
 */
final class JsonHandler implements HandlerInterface {

  public function parse(string $raw) {
    $data = trim($raw) ? json_decode($raw) : null;
    // JSON_THROW_ON_ERROR needs PHP7.3.
    if (json_last_error() !== JSON_ERROR_NONE) {
      throw new \RuntimeException('Invalid json body: ' . json_last_error_msg());
    }
    return $data;
  }

  public function dump($data): string {
    $raw = json_encode($data, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    if (json_last_error() !== JSON_ERROR_NONE) {
      throw new \RuntimeException('Invalid json body: ' . json_last_error_msg());
    }
    return $raw;
  }

}
