<?php

namespace geeks4change\treetool\Processor;

use geeks4change\treetool\Utility\NestedData;

/**
 * Class GetProcessor
 * @internal
 */
final class GetProcessor implements ProcessorInterface {

  /**
   * @var string[]
   */
  private $keys;

  public function __construct(array $keys) {
    $this->keys = $keys;
  }

  public function process($data) {
    try {
      $data = NestedData::getValue($data, $this->keys);
    } catch (\Error $e) {
      throw new \RuntimeException("Error getting key: {$e->getMessage()}", 0, $e);
    }
    return $data;
  }

}
