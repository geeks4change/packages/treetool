<?php

namespace geeks4change\treetool\Processor;

/**
 * Class TypeProcessor
 * @internal
 */
class TypeProcessor implements ProcessorInterface {

  public function process($data) {
    if (is_null($data)) {
      return 'null';
    }
    elseif (is_bool($data)) {
      return 'bool';
    }
    elseif (is_string($data)) {
      return 'string';
    }
    elseif (is_int($data) || is_float($data)) {
      return 'number';
    }
    elseif (is_array($data)) {
      return 'array';
    }
    elseif (is_object($data)) {
      return 'object';
    }
    else {
      throw new \RuntimeException("Undefined type: " . gettype($data));
    }
  }

}
