<?php

namespace geeks4change\treetool\Processor;

use geeks4change\treetool\Utility\NestedData;

/**
 * Class SetProcessor
 * @internal
 */
final class SetProcessor implements ProcessorInterface {

  /**
   * @var string[]
   */
  private $keys;

  /**
   * @var mixed
   */
  private $value;

  /**
   * @var bool
   */
  private $replace;

  public function __construct(array $keys, $value, bool $replace) {
    $this->keys = $keys;
    $this->value = $value;
    $this->replace = $replace;
  }

  public function process($data) {
    try {
      NestedData::setValue($data, $this->keys, $this->value, $this->replace);
    } catch (\Error $e) {
      throw new \RuntimeException("Error setting key: {$e->getMessage()}", 0, $e);
    }
    return $data;
  }

}
