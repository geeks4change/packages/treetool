<?php

namespace geeks4change\treetool\Processor;

/**
 * Class KeysProcessor
 * @internal
 */
class KeysProcessor implements ProcessorInterface {

  /**
   * @var string
   */
  private $separator;

  /**
   * KeysProcessor constructor.
   * @param string $separator
   */
  public function __construct(string $separator) {
    $this->separator = $separator;
  }

  public function process($data) {
    if (is_array($data) || is_object($data)) {
      $keys = array_keys((array) $data);
      // Ensure a trailing separator for the bash read command.
      return implode($this->separator, $keys) . $this->separator;
    }
    else {
      throw new \RuntimeException("Not an array or object.");
    }
  }

}
