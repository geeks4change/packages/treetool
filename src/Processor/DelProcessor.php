<?php

namespace geeks4change\treetool\Processor;

use geeks4change\treetool\Utility\NestedData;

/**
 * Class DelProcessor
 * @internal
 */
final class DelProcessor implements ProcessorInterface {

  /**
   * @var string[]
   */
  private $keys;

  /**
   * @var bool
   */
  private $clean;

  public function __construct(array $keys, bool $clean) {
    $this->keys = $keys;
    $this->clean = $clean;
  }

  public function process($data) {
    NestedData::unsetValue($data, $this->keys);
    if ($this->clean) {
      $keys = $this->keys;
      // Clean up resulting empty arrays.
      while (
        array_pop($keys)  !== null
        && NestedData::getValue($data, $keys) === []
      ) {
        NestedData::unsetValue($data, $keys);
      }
    }
    return $data;
  }

}
