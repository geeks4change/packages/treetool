<?php

namespace geeks4change\treetool\Processor;

/**
 * Interface ProcessorInterface
 * @internal
 */
interface ProcessorInterface {

  /**
   * @param array|string|int|float|null $data
   * @return array|string|int|float|null
   */
  public function process($data);

}