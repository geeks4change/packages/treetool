<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Processor\GetProcessor;
use geeks4change\treetool\Processor\ProcessorInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class GetCommand
 * @internal
 */
final class GetCommand extends CommandBase {

  protected static $defaultName = 'get';

  protected function configure() {
    parent::configure();
    $this
      ->addArgument('keys', InputArgument::IS_ARRAY , 'A sequence of keys.', [])
      ->setDescription('Get a key path.')
      ->setHelp('Example: tt get -i file.json foo bar --out-type=string')
    ;
  }

  protected function createProcessor(InputInterface $input): ProcessorInterface {
    return new GetProcessor($input->getArgument('keys'));
  }

}
