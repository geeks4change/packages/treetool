<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Processor\ProcessorInterface;
use geeks4change\treetool\Processor\SetProcessor;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class SetCommand
 * @internal
 */
final class SetCommand extends CommandBase {

  protected static $defaultName = 'set';

  protected function configure() {
    parent::configure();
    $this
      ->addOption('file', 'f', InputOption::VALUE_REQUIRED , 'The file to be changed. Sets both --in and --out.')
      ->addArgument('keys', InputArgument::IS_ARRAY , 'A sequence of keys, plus a value to set.', [])
      ->addOption('value-type', 'w', InputOption::VALUE_REQUIRED, 'The type to interpret the value string. You may need this for non-string values.', 'plain')
      ->addOption('replace', 'r', InputOption::VALUE_NONE, 'Allow replacing existing value with array or object.')
      ->setDescription('Set a key path to a value.')
      ->setHelp('Example: tt set -f file.json foo bar value')
    ;
  }

  protected function createProcessor(InputInterface $input): ProcessorInterface {
    $keys = $input->getArgument('keys');
    if (!$keys) {
      throw new \RuntimeException('Set command needs at least a value, and possibly some keys.', CommandBase::VALUE_ERROR);
    }

    $valueType = $input->getOption('value-type');
    $valueHandler = $this->createHandler($valueType, '--value-type');

    $raw = array_pop($keys);
    $data = $valueHandler->parse($raw);
    return new SetProcessor($keys, $data, $input->getOption('replace'));
  }

}
