<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Processor\KeysProcessor;
use geeks4change\treetool\Processor\ProcessorInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class KeysCommand
 * @internal
 */
class KeysCommand extends CommandBase {


  protected static $defaultName = 'keys';

  protected $defaultOutputType = 'plain';

  protected function configure() {
    parent::configure();
    $this
      ->addOption('separator', 's', InputOption::VALUE_REQUIRED, 'The keys separator, defaults to \n.', "\n")
      ->setDescription('Get the keys of the root array or object.')
      ->setHelp('Example: tt keys -i file.json')
    ;
  }

  protected function createProcessor(InputInterface $input): ProcessorInterface {
    return new KeysProcessor($input->getOption('separator'));
  }

}
