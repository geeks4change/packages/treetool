<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Processor\ProcessorInterface;
use geeks4change\treetool\Processor\TypeProcessor;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class TypeCommand
 * @internal
 */
class TypeCommand extends CommandBase {

  protected static $defaultName = 'type';

  protected $defaultOutputType = 'plain';

  protected function configure() {
    parent::configure();
    $this
      ->setDescription('Get the type of the root element.')
      ->setHelp('Example: tt type -i file.json -u plain')
    ;
  }

  protected function createProcessor(InputInterface $input): ProcessorInterface {
    return new TypeProcessor();
  }

}
