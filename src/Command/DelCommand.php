<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Processor\DelProcessor;
use geeks4change\treetool\Processor\ProcessorInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class DelCommand
 * @internal
 */
final class DelCommand extends CommandBase {

  protected static $defaultName = 'del';

  protected function configure() {
    parent::configure();
    $this
      ->addOption('file', 'f', InputOption::VALUE_REQUIRED , 'The file to be changed. Sets both --in and --out.')
      ->addArgument('keys', InputArgument::IS_ARRAY , 'A sequence of keys.', [])
      ->addOption('clean', 'c', InputOption::VALUE_NONE, 'Clean up resulting empty arrays.')
      ->setDescription('Delete a key path.')
      ->setHelp('Example: tt del -f file.json foo bar')
    ;
  }

  protected function createProcessor(InputInterface $input): ProcessorInterface {
    return new DelProcessor($input->getArgument('keys'), $input->getOption('clean'));
  }

}
