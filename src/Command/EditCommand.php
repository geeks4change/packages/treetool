<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Handler\JsonHandler;
use geeks4change\treetool\Handler\YamlHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class EditCommand extends Command {

  protected static $defaultName = 'edit';

  /**
   * @var \geeks4change\treetool\Handler\HandlerInterface
   */
  protected $jsonHandler;

  /**
   * @var \geeks4change\treetool\Handler\HandlerInterface
   */
  protected $yamlHandler;

  public function __construct(string $name = null) {
    parent::__construct($name);
    $this->jsonHandler = new JsonHandler();
    $this->yamlHandler = new YamlHandler();
  }

  protected function configure() {
    $this
      ->addArgument('file', InputArgument::REQUIRED, 'The json file to edit.')
      ->setDescription('Edit a json file as Yaml.')
      ->setHelp('Example: tt edit composer.json')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new SymfonyStyle($input, $output);
    $errorStyle = $io->getErrorStyle();

    $fileName = $input->getArgument('file');
    $tmpName = tempnam(sys_get_temp_dir(), 'tt-edit-');

    $json1 = file_get_contents($fileName);
    $data1 = $this->jsonHandler->parse($json1);
    $yaml1 = $this->yamlHandler->dump($data1);
    file_put_contents($tmpName, $yaml1);
    register_shutdown_function(function () use ($tmpName) {
      unlink($tmpName);
    });

    $process = new Process(['/usr/bin/editor', $tmpName]);
    $process->setTty(true);
    $process->run();
    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
    }
    $yaml2 = file_get_contents($tmpName);
    // @todo Consider re-editing on parse errors.
    $data2 = $this->yamlHandler->parse($yaml2);
    $json2 = $this->jsonHandler->dump($data2);
    file_put_contents($fileName, $json2);
  }


}
