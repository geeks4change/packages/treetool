<?php

namespace geeks4change\treetool\Command;

use geeks4change\treetool\Handler\JsonHandler;
use geeks4change\treetool\Handler\PlainHandler;
use geeks4change\treetool\Handler\YamlHandler;
use geeks4change\treetool\Processor\ProcessorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CommandBase
 * @internal
 */
abstract class CommandBase extends Command {

  const SUCCESS = 0;
  const VALUE_ERROR = 1;
  const READ_ERROR = 2;
  const PARSE_ERROR = 3;
  const PROCESS_ERROR = 4;
  const DUMP_ERROR = 5;
  const WRITE_ERROR = 6;

  protected $defaultOutputType = null;

  protected function configure() {
    $outTypeFallback = $this->defaultOutputType ?: 'in-type';
    $this
      ->addOption('in', 'i', InputOption::VALUE_REQUIRED , 'The input file. If omitted, reads from stdin.')
      ->addOption('out', 'o', InputOption::VALUE_REQUIRED , 'The output file. If omitted, writes to stdout.')
      ->addOption('in-type', 't', InputOption::VALUE_REQUIRED, 'File type (json / yml / plain), defaults to file extension, with a fallback to yml which also understands json. In-type plain reads the complete file/stdin content to a single string. Defaults to in file extension, then out file extension, then yml.')
      ->addOption('out-type', 'u', InputOption::VALUE_REQUIRED, "File type (json / yml / plain), only needed if no same as file extension. Out-type plain validates the output is a string or number and outputs it unquoted. Otherwise it returns an error code and produces no output. Defaults to out file extension, then $outTypeFallback.")
      ->addOption('suppress-trailing-eol', null, InputOption::VALUE_NONE, 'Suppress trailing EOL in the output.')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new SymfonyStyle($input, $output);
    $errorStyle = $io->getErrorStyle();
    try {
      $inFileName = $input->getOption('in');
      $outFileName = $input->getOption('out');
      if ($input->hasOption('file')) {
        $fileName = $input->getOption('file');
        if ($fileName) {
          if ($inFileName || $outFileName) {
            throw new \RuntimeException("The --in and --out option can not be used with --file.", static::VALUE_ERROR);
          }
          $inFileName = $outFileName = $fileName;
        }
      }

      $inType = $input->getOption('in-type')
        ?: pathinfo($inFileName, PATHINFO_EXTENSION)
        ?: pathinfo($outFileName, PATHINFO_EXTENSION)
        ?: 'yml';
      $outType = $input->getOption('out-type')
        ?: pathinfo($outFileName, PATHINFO_EXTENSION)
        ?: ($this->defaultOutputType ?: $inType);

      $inHandler = $this->createHandler($inType, '--in-type');
      $outHandler = $this->createHandler($outType, '--out-type');

      $errorStyle->writeln("Debug: --in '$inFileName' --out '$outFileName' --in-type '$inType' --outType '$outType'",
        OutputInterface::VERBOSITY_DEBUG);

      $processor = $this->createProcessor($input);

      // Do the work.
      $raw = file_get_contents($inFileName ?: 'php://stdin');
      if ($raw === FALSE) {
        throw new \RuntimeException('Can not read file', static::READ_ERROR);
      }

      try {
        $data = $inHandler->parse($raw);
      } catch (\RuntimeException $e) {
        throw new \RuntimeException($e->getMessage(), static::PARSE_ERROR, $e);
      }
      try {
        $data = $processor->process($data);
      } catch (\RuntimeException $e) {
        throw new \RuntimeException($e->getMessage(), static::PROCESS_ERROR, $e);
      }
      try {
        $raw = $outHandler->dump($data);
      } catch (\RuntimeException $e) {
        throw new \RuntimeException($e->getMessage(), static::DUMP_ERROR, $e);
      }

      if (!$input->getOption('suppress-trailing-eol')) {
        $raw .= "\n";
      }

      $writeSuccess = file_put_contents($outFileName ?: 'php://stdout', $raw);
      if ($writeSuccess === FALSE) {
        throw new \RuntimeException('Can not write file', static::WRITE_ERROR);
      }
    } catch (\RuntimeException $e) {
      $errorStyle->error($e->getMessage());
      return $e->getCode();
    }

    // If not --quiet, ensure that prompt is on a new line.
    if (!$outFileName && posix_isatty(STDOUT)) {
      $io->writeln('', OutputInterface::OUTPUT_NORMAL);
    }

    return static::SUCCESS;
  }

  /**
   * @param string $type
   * @param string $name
   * @return \geeks4change\treetool\Handler\HandlerInterface|null
   */
  protected function createHandler(string $type, string $name) {
    if ($type === 'yml') {
      return new YamlHandler();
    }
    elseif ($type === 'json') {
      return new JsonHandler();
    }
    elseif ($type === 'plain') {
      return new PlainHandler();
    }
    throw new \RuntimeException("Invalid $name: $type", static::VALUE_ERROR);
  }

  abstract protected function createProcessor(InputInterface $input): ProcessorInterface;

}
